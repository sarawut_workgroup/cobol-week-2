       IDENTIFICATION DIVISION. 
       PROGRAM-ID. BIRTHDATE.
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01 BIRTHDATE.
          02 YEAROFBIRTH.
             03 CENTURYOB  PIC 99.
             03 YEAROB     PIC 99.
          02 MONTHOFBIRTH  PIC 99.
          02 DAYOFBIRTH    PIC 99.

       PROCEDURE DIVISION.
           MOVE 19750215 TO BIRTHDATE
           DISPLAY "Month is = " MONTHOFBIRTH
           DISPLAY "Century of birth is = " CENTURYOB
           DISPLAY "Year of birth is = " YEAROFBIRTH
           DISPLAY DAYOFBIRTH "/" MONTHOFBIRTH "/" YEAROFBIRTH
           MOVE ZEROS TO YEAROFBIRTH
           DISPLAY "Birth date = " BIRTHDATE.